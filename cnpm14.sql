create database CNPMN14
go
use CNPMN14
create table khach(
	id_user nvarchar(8) not null,
	taikhoan nvarchar(30) not null,
	matkhau nvarchar (30) not null,
)

create table chitietkhach(
	id_user nvarchar(8) not null,
	name nvarchar(30) not null,
	ngaysinh datetime default getdate(),
	gioitinh bit default 1,
	diachar nvarchar(100),
	sdt nvarchar(10),
	emails nvarchar(30)
)

create table nha(
	id_nha nvarchar(8) not null,
	id_chunha nvarchar(10) not null,
	diachi nvarchar(100),
	thongtin nvarchar(500),
	khuvuc nvarchar(50),
	chatluong nvarchar(100)
)
create table chitietmua(
	id_nha nvarchar(8) not null,
	id_user nvarchar(8) not null,
	ngaymua datetime default getdate(),
	
	)
create table chitietnha(
	id_nha nvarchar(8) not null,
	anh nvarchar(300),
	giatien smallmoney
)

create table chunha(
	id_chunha nvarchar(10) not null,
	hoten nvarchar(30) not null,
	ngaysinh datetime default getdate(),
	gioitinh bit default 1,
	diachi nvarchar(100),
	sdt nvarchar(10),
	cmt nvarchar(12),
	email nvarchar(20)
)

create table nhanvien(
	id_nhanvien nvarchar(10) not null,
	hoten nvarchar(30) not null,
	ngaysinh datetime default getdate(),
	gioitinh bit default 1,
	diachi nvarchar(100),
	sdt nvarchar(10),
	cmt nvarchar(12),
	email nvarchar(20)
)
alter table khach add constraint PK_khach primary key(id_user)
alter table nha add constraint Pk_nha primary key (id_nha)
alter table chunha add constraint PK_chunha primary key (id_chunha)
alter table nhanvien add constraint PK_nhanvien primary key (id_nhanvien)
alter table chitietkhach 
add constraint FK_chitietkhach
Foreign key(id_user)
references dbo.khach(id_user)

alter table chitietmua
add constraint Fk_khach
Foreign key(id_user)
references khach(id_user)

alter table chitietmua
add constraint Fk_ttnha
Foreign key(id_nha)
references dbo.nha(id_nha)

alter table dbo.chitietnha
add constraint Fk_chitiet
Foreign key(id_nha)
references dbo.nha(id_nha)

alter table nha 
add constraint Fk_chunha
foreign key(id_chunha)
references chunha(id_chunha)